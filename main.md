# Financialisation

## Gliederung

- Was bezeichnet der Begriff?
- In welchen Kontext ist der Begriff und das Thema einzuordnen?
- Mit welchen Begriffen steht das Thema in engem Zusammenhang?

### Geschichte

- Finanzialisierung beschreibt eine historische ökonomische Entwicklung
  - Wie sieht die Zeitleiste aus?
  - Welche wesentlichen Ereignisse und Eckdaten bestimmen diese Entwicklung?

### Effekte und Auswirkungen

- Welche ökonomischen Erscheinungen werden mit Finanzialisierung in Verbindung gebracht?
- Welche sozialen und kulturellen Effekte werden auf die Finanzialisierung zurückgeführt?

### Diskussion

- Finanzialisierung kann bewertet werden
  - Welche Wertungen existieren, von wem kommen diese?

## Resourcen

- [Wikipedia DE](https://de.wikipedia.org/wiki/Finanzmarkt-Kapitalismus#Finanzialisierung)

## Auswerten

- [Sammelband 'Capitalization'](https://www.pressesdesmines.com/produit/capitalization/)